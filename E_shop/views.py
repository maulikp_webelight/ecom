from atexit import register
from crypt import methods
from email import message
import email
from multiprocessing import context
from telnetlib import STATUS
from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from store_app.models import Order, Product, Categories, FILTER_PRICE, Color, Brand, contact_us, orderiteam, Tag
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from cart.cart import Cart
import razorpay
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator

# reset pass


client = razorpay.Client(auth=(settings.RAZORPAY_KEY_ID, settings.RAZORPAY_KEY_SECRET))


def base(request):
    return render(request, 'main/base.html')


def home(request):
    product = Product.objects.filter(status="Publish")
    new_product=Product.objects.filter(status="Publish")[:4]
    context = {
        'product': product,
        'new_prod': new_product
    }
    return render(request, 'main/index.html', context)


def product(request):
    product = Product.objects.filter(status="Publish")
    categories = Categories.objects.all()
    filter_price = FILTER_PRICE.objects.all()
    color = Color.objects.all()
    brand = Brand.objects.all()
    tag = Tag.objects.all()

    CATID = request.GET.get('categories')
    FILTER_PRICE_ID = request.GET.get('filter_price')
    COLOR_ID = request.GET.get('color')
    BRAND_ID = request.GET.get('brand')
    tag_id = request.GET.get('tag')

    ATOZID = request.GET.get('ATOZ')
    ZTOAID = request.GET.get('ZTOA')
    LOWTOHIGHID = request.GET.get('LOWTOHIGH')
    HIGHTOLOWID = request.GET.get('HIGHTOLOW')
    NEW_PRODUCTID = request.GET.get('NEWPRODUCT')
    OLD_PRODUCTID = request.GET.get('OLDPRODUCT')


    #Pagination Start
    product=Product.objects.filter(status="Publish")
    paginator = Paginator(product,4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    



    #Pagination End
    

    if CATID:
        product = Product.objects.filter(Categories=CATID, status="Publish")

    elif FILTER_PRICE_ID:
        product = Product.objects.filter(filter_price=FILTER_PRICE_ID, status="Publish")

    elif COLOR_ID:
        product = Product.objects.filter(color=COLOR_ID, status="Publish")

    elif BRAND_ID:
        product = Product.objects.filter(brand=BRAND_ID)

    elif tag_id:
        product = Product.objects.filter(tag=tag_id)

    elif ATOZID:
        product = Product.objects.filter(status="Publish").order_by('name')

    elif ZTOAID:
        product = Product.objects.filter(status='Publish').order_by('-name')

    elif LOWTOHIGHID:
        product = Product.objects.filter(status='Publish').order_by('price')

    elif HIGHTOLOWID:
        product = Product.objects.filter(status='Publish').order_by('-price')

    elif NEW_PRODUCTID:
        product = Product.objects.filter(status='Publish', condition='New')

    elif OLD_PRODUCTID:
        product = Product.objects.filter(status='Publish', condition='Old')


    else:
        product = Product.objects.filter(status="Publish")



    context = {
        'product': product,
        'categories': categories,
        'f_price': filter_price,
        'color': color,
        'brand': brand,
        'tag': tag,
       
    }

    return render(request, 'main/product.html', context)


def search(request):
    query = request.GET.get('query')
    product = Product.objects.filter(name__icontains=query)

    context = {
        'product': product
    }

    return render(request, 'main/search.html', context)


def product_details_page(request, id):
    prod = Product.objects.filter(id=id).first()

    context = {
        'prod': prod,
    }
    return render(request, 'main/product_single.html', context)


def contact(request):
    if request.method == "POST":
        name = request.POST.get('name')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')

        contact = contact_us(
            name=name,
            email=email,
            subject=subject,
            message=message,
        )

        subject = subject
        message = message
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, ['maulikg0987@gmail.com'])
        contact.save()
        return redirect('home')

    return render(request, 'main/contact.html')


def handle_register(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        pass1 = request.POST.get('pass1')
        pass2 = request.POST.get('pass2')
        if pass1 != pass2:
            messages.warning(request, 'password does not match')
            return redirect('register')
        elif User.objects.filter(email=email).exists():
            messages.warning(request, 'Email is already exists')
            return redirect('register')
        else:
            customer = User.objects.create_user(username, email, pass1)
            customer.first_name = first_name
            customer.last_name = last_name
            customer.save()
            messages.success(request, "User Has Been Register Successfully")
            return redirect('register')

    return render(request, 'registration/auth.html')


def handle_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:

            login(request, user)

            return redirect('home')
        else:
            messages.warning(request, "Invalid username or password")
            return redirect('login')

    return render(request, 'registration/auth.html')


def handle_logout(request):
    logout(request)

    return redirect('home')


# cart
@login_required(login_url="/login/")
def cart_add(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("home")


@login_required(login_url="/login/")
def item_clear(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.remove(product)
    return redirect("cart_detail")


@login_required(login_url="/login/")
def item_increment(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("cart_detail")


@login_required(login_url="/login/")
def item_decrement(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.decrement(product=product)
    return redirect("cart_detail")


@login_required(login_url="/login/")
def cart_clear(request):
    cart = Cart(request)
    cart.clear()
    return redirect("cart_detail")


@login_required(login_url="/login/")
def cart_detail(request):
    return render(request, 'cart/cartdetails.html')


def cheak_out(request):
    amount_str = request.POST.get('amount')
    print(amount_str)
    print(type(amount_str))
    amount_float = (float(amount_str))
    amount = (int(amount_float))
    print(amount)

    payment = client.order.create(
        {
            "amount": amount,
            "currency": "INR",
            "payment_capture": 1
        }
    )
    order_id = payment['id']
    context = {
        "order_id": order_id,
        "payment": payment
    }
    return render(request, 'cart/cheakout.html', context)


def place_order(request):
    if request.method == 'POST':
        uid = request.session.get('_auth_user_id')
        USER = User.objects.get(id=uid)
        cart = request.session.get('cart')
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        country = request.POST.get('country')
        address = request.POST.get('address')
        city = request.POST.get('city')
        state = request.POST.get('state')
        postcode = request.POST.get('postcode')
        phone = request.POST.get('phone')
        email = request.POST.get('email')
        message = request.POST.get('message')
        amount = request.POST.get('cart_total_amount')

        order_id = request.POST.get('order_id')
        payment = request.POST.get('payment')

        context = {
            'order_id': order_id
        }

        order = Order(
            user=USER,
            firstname=firstname,
            lastname=lastname,
            county=country,
            address=address,
            city=city,
            state=state,
            postcode=postcode,
            phone=phone,
            email=email,
            additional_info=message,
            amount=amount,
            payment_id=order_id

        )
        order.save()

        for i in cart:
            a = (int(cart[i]['price']))
            b = cart[i]['quantity']
            total = a * b

            item = orderiteam(
                order=order,
                product=cart[i]['name'],
                image=cart[i]['image'],
                quantity=cart[i]['quantity'],
                price=cart[i]['price'],
                total=total
            )
            item.save()

        return render(request, 'cart/placeorder.html', context)


@csrf_exempt
def sucess(request):
    if request.method == 'POST':
        a = request.POST
        order_id = ' '
        for key, val in a.items():
            if key == 'razorpay_order_id':
                order_id = val
                break
        user = Order.objects.filter(payment_id=order_id).first()
        user.paid = True
        user.save()

    return render(request, 'cart/thankyou.html')

    # demo for invoice#


def invoice(request):
    order = Order.objects.filter(paid=True)[:1]
    item = orderiteam.objects.all()[:1]
    context = {
        'order': order,
        'item': item
    }
    return render(request, 'cart/invoice.html', context)


def changepassword(request):
    if request.method == "POST":
        newpass = request.POST.get('newpassword')
        u = User.objects.get(username=request.user.username)
        u.set_password(newpass)
        u.save()
        messages.success(request, "Password change sucessfully")
        return redirect('home')
    return render(request, 'registration/changepassword.html')


# reset password
def my_account(request):
    user = User.objects.get(id=request.user.id)

    context = {
        "user": user
    }
    return render(request, 'main/my_account.html', context)


#About_us

def about_us(request):
    return render(request,'main/about_us.html')