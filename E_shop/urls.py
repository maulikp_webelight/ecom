from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', views.home, name="home"),
                  path('base/', views.base, name="base"),
                  path('product/', views.product, name="product"),

                  path('search/', views.search, name='search'),
                  path('product_details/<str:id>', views.product_details_page, name='product_detail'),
                  path('contact/', views.contact, name='contact'),

                  path('register/', views.handle_register, name='register'),
                  path('login/', views.handle_login, name='login'),
                  path('logout/', views.handle_logout, name='logout'),

                  # cart
                  path('cart/add/<int:id>/', views.cart_add, name='cart_add'),
                  path('cart/item_clear/<int:id>/', views.item_clear, name='item_clear'),
                  path('cart/item_increment/<int:id>/', views.item_increment, name='item_increment'),
                  path('cart/item_decrement/<int:id>/', views.item_decrement, name='item_decrement'),
                  path('cart/cart_clear/', views.cart_clear, name='cart_clear'),
                  path('cart/cart-detail/', views.cart_detail, name='cart_detail'),
                  path('cart/cheakout/', views.cheak_out, name='cheakout'),
                  path('cart/cheakout/placeorder/', views.place_order, name='placeorder'),
                  path('sucess/', views.sucess, name='sucess'),
                  ##
                  path('invoice/', views.invoice, name='invoice'),
                  # password Manage
                  path('changepassword', views.changepassword, name='changepassword'),
                  # reset password
                  path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
                  path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
                  path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(),
                       name='password_reset_confirm'),
                  path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

                  # my account
                  path('myaccount/', views.my_account, name='myaccount'),
                  path('about_us/',views.about_us,name="about_us"),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
